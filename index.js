"use strict";
/*
Теоретичні питання
1. Опишіть своїми словами, що таке метод об'єкту.
Метод об'єкту  - це функція, яка є властивістю об'єкта. В тілі функції ми можемо 
виконувати якісь дії, які ми придумали самі. Або є вбудовані методи. Наприклад, метод
toUpperCase() , який слугує для перетворення тексту в верхній реєстр. 

2. Який тип даних може мати значення властивості об'єкта?
Будь-який тип даних. 

3. Об'єкт це посилальний тип даних. Що означає це поняття?
Посилальний тип даних - це внутрішній тип мови. Це означає, що у властивості зберігається
не точне значення цієї властивості, а спеціальне значення "посилального типу", в якому 
зберігаються значення властивості і сам об'єкт. */

function User() {
    this.firstName = prompt("Ваше имя?");
    this.lastName = prompt("Ваша фамилия");
    while(!this.firstName && !this.lastName) {
      this.firstName = prompt("Вы не ввели ваше имя?");
      this.lastName = prompt("Вы не ввели вашу фамилию");
    }

    this.getLogin = function() {
    console.log(this.firstName[0].toLowerCase() + this.lastName.toLowerCase());
     };
  };
  
let newUser = new User();
newUser.getLogin();